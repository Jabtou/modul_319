package ch.bbw;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner keyboardInput = new Scanner(System.in);
        double mpy = 60*24*365.25;
        System.out.print("Geben Sie Ihren Pulsschlag ein: ");
        double puls = keyboardInput.nextDouble();
        System.out.print("Geben Sie Ihr Alter ein: ");
        double alter = keyboardInput.nextInt();
        double hbpy = puls*mpy*alter;
        System.out.printf("Ihr Puls ist %.0f Ihr Alter ist %.0f Ihr Herz hat bis jetzt %.0f mal geschlagen. ",puls,alter,hbpy);
    }
}
