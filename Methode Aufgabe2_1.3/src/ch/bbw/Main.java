package ch.bbw;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Plates plates = new Plates();
        Scanner KeyboardInput = new Scanner(System.in);
        int antwort = 0;
        while (antwort != 1) {
            System.out.print("Gebene sie die Breite des Raumes ein in m: ");
            double widthRoom = KeyboardInput.nextDouble();
            System.out.print("Gebene sie die Länge des Raumes ein in m: ");
            double lengthRoom = KeyboardInput.nextDouble();
            System.out.print("Gebene sie die Länge der Platte ein in m: ");
            double lengthPlate = KeyboardInput.nextDouble();
            double NrOfPlates = plates.NrOfPlates(lengthRoom, widthRoom, lengthPlate);
            double Surface = plates.Surface(widthRoom, lengthRoom);
            double Price = plates.Price(widthRoom, lengthRoom);
            System.out.println("Raumlänge in m : " + lengthRoom);
            System.out.println("Raumbreite in m : " + widthRoom);
            System.out.println("Länge der Platten in m : " + lengthPlate);
            System.out.println("Sie benötigen " + NrOfPlates + " Bodenplatten");
            System.out.println("Die Raumfläche beträgt " + Surface + "m");
            System.out.println("Gesamtpreis: " + Price + " Fr");
            System.out.println("Noch einmal berechnen? (Ja=0;Nein=1)");
            antwort = KeyboardInput.nextInt();



        }
    }
}
