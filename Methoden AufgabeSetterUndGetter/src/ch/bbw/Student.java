package ch.bbw;

import java.util.Date;
import java.util.List;

public class Student {
    private String Name;
    private String Lastname;
    private Date Birthdate;
    private String Residence;
    private String BoughtBooks;
    private boolean Velo;
    private boolean vaccinated;



    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public Date getBirthdate() {
        return Birthdate;
    }

    public void setBirthdate(Date birthdate) {
        Birthdate = birthdate;
    }

    public String getResidence() {
        return Residence;
    }

    public void setResidence(String residence) {
        Residence = residence;
    }

    public String getBoughtBooks() {
        return BoughtBooks;
    }

    public void setBoughtBooks(String boughtBooks) {
        BoughtBooks = boughtBooks;
    }

    public boolean isVelo() {
        return Velo;
    }

    public void setVelo(boolean velo) {
        Velo = velo;
    }

    public boolean isVaccinated() {
        return vaccinated;
    }

    public void setVaccinated(boolean vaccinated) {
        this.vaccinated = vaccinated;
    }
}
