package ch.bbw;

import java.math.MathContext;

public class Geometrics {
    public double berechneRechtecksFlaeche(double breite, double hoehe) {
        return hoehe * breite;
    }

    public double berechneKreisFlaeche(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }

    public double bercheneKreisUmfang(double radius) {
        return radius * Math.PI * 2;
    }

    public double berechneZylinderObreflaeche(double radius, double hoehe) {
        double Stirnflaeche = berechneKreisFlaeche(radius);
        double Mantellaenge = bercheneKreisUmfang(radius);
        double Mantelflaeche = berechneRechtecksFlaeche(Mantellaenge, hoehe);
        return 2 * Stirnflaeche + Mantelflaeche;


    }
}
